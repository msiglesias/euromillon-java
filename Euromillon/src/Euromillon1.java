import java.util.Random;
import java.util.Scanner;
import java.io.*;

public class Euromillon1 {

	
	public static void main(String[] args) {
		Scanner leer= new Scanner(System.in);
		Random r= new Random();
		System.out.println("Introduce el precio de la estrella");
		double p1=leer.nextDouble();
		System.out.println("Introduce el precio del numero");
		double p2=leer.nextDouble();
		int i;
		int columnas;
		int numerosAcertados=0;
		int estrellasAcertadas=0;
		double premio=0;
		int[] numeros = new int [5];//array de enteros que contiene los numeros ganadores
		int [] estrellas = new int [2];//array de enteros que contiene las estrellas ganadoras
		int[] misnumeros=new int [5];//array de enteros que contine mis numeros del boleto
		int[] misestrellas=new int[2];//array de enteros que contiene mis estrellas del boleto
		String cadena1;
		
		generarCombinacionGanadora(numeros, estrellas);
		
		
		System.out.println("Bienvenido a su Euromillon online");
		System.out.printf(" Hoy el numero acertado se paga a: %.2f euros\n"+" Y la estrella acertada se paga a: %.2f euros\n",p2,p1);
		System.out.println("�Cuantas columnas desea jugar?");
		columnas=leer.nextInt();
		
		System.out.println("�Como desea que se genere la combinacion?");
		cadena1= leer.next();
	    while (!cadena1.equals("manual") && !cadena1.equals ("automatico")){
			System.out.println("�Como desea que se genere la combinacion?");
			cadena1= leer.next();
		}
			
		for (i=1;i<=columnas;i++){
			
			switch (cadena1) {
	        case "automatico":
				System.out.println("columna "+i);
	        	for (int x=0;x<=4;x++){//introduce los numeros escogidos en mi boleto
	    			misnumeros[x]= r.nextInt(50)+1;
	    		}
	    		
	    		for (int y=0;y<=1;y++){//se introducen las estrellas escogidas en mi boleto
	    			misestrellas[y] = r.nextInt(9)+1;
	    		}
	    		
	    		while (micombinacionRepetida (misnumeros,misestrellas)){
	    			for (int x=0;x<=4;x++){//introduce los numeros escogidos en mi boleto
	        			misnumeros[x]= r.nextInt(50)+1;
	        		}
	        		
	        		for (int y=0;y<=1;y++){//se introducen las estrellas escogidas en mi boleto
	        			misestrellas[y] = r.nextInt(9)+1;
	        		}
					
					
				}
				
	    	    
	    		
				System.out.println("�Desea ver la combinacion escogida?");
				String cadena3 = leer.next();
			
				FileWriter fichero = null;
		        PrintWriter pw = null;
		        try
		        {
		            fichero = new FileWriter("boleto.txt");
		            pw = new PrintWriter(fichero);
		            
             
		            pw.println("Apuersta: "+i);
		            pw.println("Numeros:");
		            for (int x=0;x<=4;x++){//recorre todos mis numeros del array y los muestra por pantalla
		            	pw.println(+misnumeros[x]);
		            }    
		            pw.println("Estrellas:");
		            for (int x=0;x<=1;x++){//recorre todas mis estrellas del array y las muestra por pantalla
					    pw.println(misestrellas[x]);
					}
		        } catch (Exception e) {
		            e.printStackTrace();
		        } finally {
		           try {
		           // Nuevamente aprovechamos el finally para 
		           // asegurarnos que se cierra el fichero.
		           if (null != fichero)
		              fichero.close();
		           } catch (Exception e2) {
		              e2.printStackTrace();
		           }
		        }
				
				
				
				
				if (cadena3.equals("si")){
					System.out.println("Los numeros escogidos en la columna "+i+" son:");
					for (int x=0;x<=4;x++){//recorre todos mis numeros del array y los muestra por pantalla
						System.out.println(misnumeros[x]);
					}
						
						
					System.out.println("Las estrellas escogidas en la columna "+i+" son:");
					for (int x=0;x<=1;x++){//recorre todas mis estrellas del array y las muestra por pantalla
						System.out.println(misestrellas[x]);
					}
				}
				
				
				for (int x=0;x<=4;x++){//compara mis numeros de mi boleto con el boleto premiado
					for (int y=0;y<=4;y++){//boleto premiado
						if (misnumeros[x]==numeros[y]){//compara si los numeros son los premiados
							System.out.println("Ha acertado el n�mero: "+misnumeros[x]+" de la "+ i+" apuesta" );
							++numerosAcertados;
							break;
						}
					}
				}
				
				for (int x=0;x<=1;x++){//compara mis estrellas de mi boleto con el boleto premiado
					for (int y=0;y<=1;y++){//boleto premiado
						if (misestrellas[x]==estrellas[y]){//compara si las estrellas son las premiados
							System.out.println("Ha acertado las estrellas: "+misestrellas[x]+" de la "+i+" apuesta");
							++estrellasAcertadas;//se incrementan el nuero de estrellas acertadas
							break;
						}
					}
				}
				break;
				
	        
	        case "manual": 
	        	System.out.println("columna "+i);
				System.out.println("Escoja 5 numeros del 1 al 50");
				
				for (int x=0;x<=4;x++){//array por la que seleccionamos nuestros numeros
					misnumeros[x]=leer.nextInt();
				}
				
				
				System.out.println("Escoja 2 estrellas del 0 al 9");
				for (int x=0;x<=1;x++){//array por la que seleccionamos nuestras estrellas
					misestrellas[x]=leer.nextInt();
				}
				
				
	    		while (micombinacionRango (misnumeros,misestrellas)){
	    			System.out.println("Dato fuera de rango. Por favor introduzca el valor nuevo.");
	    			System.out.println("Escoja 5 numeros del 1 al 50");
	    			
	    			for (int x=0;x<=4;x++){//array por la que seleccionamos nuestros numeros
	    				misnumeros[x]=leer.nextInt();
	    			}
	    			
	    			
	    			System.out.println("Escoja 2 estrellas del 0 al 9");
	    			for (int x=0;x<=1;x++){//array por la que seleccionamos nuestras estrellas
	    				misestrellas[x]=leer.nextInt();
	    			}while (micombinacionRepetida (misnumeros,misestrellas)){
	    				System.out.println("Algun numero esta repetido");
	    				System.out.println("Escoja 5 numeros del 1 al 50");
	    				
	    				for (int x=0;x<=4;x++){//array por la que seleccionamos nuestros numeros
	    					misnumeros[x]=leer.nextInt();
	    				}
	    				
	    				
	    				System.out.println("Escoja 2 estrellas del 0 al 9");
	    				for (int x=0;x<=1;x++){//array por la que seleccionamos nuestras estrellas
	    					misestrellas[x]=leer.nextInt();
	    				}
	    				
	    			}
	    			
	    			
	    		}
	    		
	    		
	    		
	    		
	    		System.out.println("Todos los datos introducidos estan dentro del rango correspondiente");
				System.out.println("�Desea ver la combinacion escogida?");
				String cadena2 = leer.next();
				
				FileWriter archivo = null;
		        PrintWriter bw = null;
		        try
		        {
		            archivo = new FileWriter("boleto.txt");
		            bw = new PrintWriter(archivo);
		            
             
		            bw.println("Apuersta: "+i);
		            bw.println("Numeros:");
		            for (int x=0;x<=4;x++){//recorre todos mis numeros del array y los muestra por pantalla
		            	bw.println(+misnumeros[x]);
		            }    
		            bw.println("Estrellas:");
		            for (int x=0;x<=1;x++){//recorre todas mis estrellas del array y las muestra por pantalla
					    bw.println(misestrellas[x]);
					}
		        } catch (Exception e) {
		            e.printStackTrace();
		        } finally {
		           try {
		           // Nuevamente aprovechamos el finally para 
		           // asegurarnos que se cierra el fichero.
		           if (null != archivo)
		              archivo.close();
		           } catch (Exception e2) {
		              e2.printStackTrace();
		           }
		        }
				
				if (cadena2.equals("si")){
					System.out.println("Los numeros escogidos en la columna "+i+" son:");
					for (int x=0;x<=4;x++){//muestra los numeros escogidos en el array
						System.out.println(misnumeros[x]);
					}
						
						
					System.out.println("Las estrellas escogidas en la columna "+i+" son:" );
					for (int x=0;x<=1;x++){//muestra las estrellas escogidas en el array
						System.out.println(misestrellas[x]);
					}
				}
				
				for (int x=0;x<=4;x++){//compara mis numeros de mi boleto con el boleto premiado
					for (int y=0;y<=4;y++){//boleto premiado
						if (misnumeros[x]==numeros[y]){//compara si los numeros son los premiados
							System.out.println("Ha acertado el n�mero: "+misnumeros[x]+" de la "+ i+" apuesta" );
							++numerosAcertados;
							break;
						}
					}
				}
				
				for (int x=0;x<=1;x++){//compara mis estrellas de mi boleto con el boleto premiado
					for (int y=0;y<=1;y++){//boleto premiado
						if (misestrellas[x]==estrellas[y]){//compara si las estrellas son las premiados
							System.out.println("Ha acertado las estrellas: "+misestrellas[x]+" de la "+i+" apuesta");
							++estrellasAcertadas;//se incrementan el nuero de estrellas acertadas
							break;
						}
					}
				}
				
				
			
			
			}//fin switch
			}//fin for
		
		System.out.println("La combinacion ganadora es:");
		System.out.println("numeros:");
		for (int x=0;x<=4;x++){//muestra los numeros ganadores del array
			System.out.println(numeros[x]);
		}
		System.out.println("estrellas:");
		for (int x=0;x<=1;x++){//muestra las estrellas ganadoras del array
			System.out.println(estrellas[x]);
		}
		if ((numerosAcertados==0)&&(estrellasAcertadas==0)){//compara los numeros acertados totales
			System.out.println("Lo sentimos. Boleto no premiado.");
		}
		else{
			premio=numerosAcertados*p2+estrellasAcertadas*p1;
			System.out.printf("El usuario ha ganado: %.2f euros. Enhorabuena\n",premio);
		}
		
		System.out.println("Gracias por usar nuestro programa. Hasta la pr�xima.");
		
	}//fin main
	
	
	public static void generarCombinacionGanadora(int [] numeros, int [] estrellas){
		
		generaCombinacion(numeros);
		
		while (combinacionRepetida(numeros)){
			generaCombinacion(numeros);
		}
		
		generaCombinacion(estrellas);
		
		while (combinacionRepetida(estrellas)){
			generaCombinacion(estrellas);
		}
	}
	
	
	public static void generaCombinacion(int [] combinacion){
		Random r= new Random();
		int numeroMaximo=50;  // numeroMaximo nos indica el numero m�ximo a generar en la combinaci�n
		
		if (combinacion.length==2){  // si la longitud de la combinaci�n de entrada es 2, es una estrella
			numeroMaximo=9;
		}
		
		for (int x=0;x<=combinacion.length-1;x++){  // recorremos desde 0 hasta la longitud del array de entrada
			combinacion[x]= r.nextInt(numeroMaximo)+1;
		}		
	}
	
	
	
	
	public static boolean combinacionRepetida (int [] numeros){
		for (int x=0;x<=numeros.length-2;x++){//se comprueba que los numeros no esten repetidos
			for (int y=x+1;y<=numeros.length-1;y++){
				if (numeros[x]==numeros[y]){
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean micombinacionRepetida (int [] misnumeros,int[] misestrellas){
		for (int x=0;x<=3;x++){//se comprueba que los numeros no esten repetidos
			for (int y=x+1;y<=4;y++){
				if (misnumeros[x]==misnumeros[y]){
					return true;
				}
			}
		
		}if (misestrellas[0]==misestrellas[1]){//se comprueba que las estrellas no esten repetidas
			return true;
		}
		return false;
	}
	
	public static boolean micombinacionRango (int[]misnumeros, int [] misestrellas){

		for (int x=0;x<=4;x++){//comprueba que los numeros esten dentro del rango
			if ((misnumeros[x]<1) || (misnumeros[x]>50)){
				return true;
				
			}
		}for (int x=0;x<=1;x++){//comprueba que las estrellas esten dentro del rango
			if ((misestrellas[x]<0) || (misestrellas[x]>9)){
				return true;
			}
			
		}
		return false;
	}


}//fin class
